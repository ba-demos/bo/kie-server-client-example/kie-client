package client;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import employeerostering.employeerostering.Employee;
import employeerostering.employeerostering.EmployeeRoster;
import employeerostering.employeerostering.Shift;
import employeerostering.employeerostering.Skill;
import employeerostering.employeerostering.Timeslot;

/**
 * LocalKieContainerMain
 */
public class LocalKieContainerMain {

    private static final String CONFIG_FILE = "employeerostering/employeerostering/EmployeeRosteringSolverConfig.solver.xml";

    public static void main(String[] args) {
        LocalKieContainerMain main = new LocalKieContainerMain();
        main.solve();
    }

    public void solve() {
        KieContainer kieContainer = KieServices.Factory.get().getKieClasspathContainer(EmployeeRoster.class.getClassLoader());
        SolverFactory<EmployeeRoster> solverFactory =
                SolverFactory.createFromKieContainerXmlResource(kieContainer, CONFIG_FILE);
        Solver<EmployeeRoster> solver = solverFactory.buildSolver();

        solver.solve(getSolution());
    }

    private EmployeeRoster getSolution() {
        Skill skill = new Skill("chess");
        Employee employee = new Employee("Matej",
                                         Arrays.asList(skill));
        Timeslot timeslot = new Timeslot(LocalDateTime.of(2017,
                                                          1,
                                                          1,
                                                          0,
                                                          0),
                                         LocalDateTime.of(2017,
                                                          1,
                                                          1,
                                                          1,
                                                          0));
        Shift shift = new Shift(timeslot,
                                skill);
        return new EmployeeRoster(Arrays.asList(employee),
                                  Arrays.asList(shift),
                                  Arrays.asList(skill),
                                  Arrays.asList(timeslot),
                                  Collections.emptyList(),
                                  Collections.emptyList(),
                                  null);
    }
}
