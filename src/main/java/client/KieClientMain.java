package client;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieContainerResource;
import org.kie.server.api.model.KieContainerResourceList;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.api.model.instance.SolverInstance;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.SolverServicesClient;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import employeerostering.employeerostering.DayOffRequest;
import employeerostering.employeerostering.Employee;
import employeerostering.employeerostering.EmployeeRoster;
import employeerostering.employeerostering.Shift;
import employeerostering.employeerostering.ShiftAssignment;
import employeerostering.employeerostering.Skill;
import employeerostering.employeerostering.Timeslot;

public class KieClientMain {

	final static Logger log = LoggerFactory.getLogger(KieClientMain.class);

	private static final String URL = "http://localhost:8080/kie-server/services/rest/server";
	private static final String user = System.getProperty("username", "donato");
	private static final String password = System.getProperty("password", "donato");

	// CONTAINER ID even known as deployment unit
	private static final String CONTAINER_ID = "employeerostering_1.0.0-SNAPSHOT";
	private static final String CONFIG_FILE = "employeerostering/employeerostering/EmployeeRosteringSolverConfig.solver.xml";
	private static final String SOLVER_ID = "EmployeeRosteringSolver";

	public static void main(String[] args) {
		KieClientMain clientApp = new KieClientMain();

		long start = System.currentTimeMillis();

		clientApp.solve();

		long end = System.currentTimeMillis();
		System.out.println("elapsed time: " + (end - start));
	}

	@SuppressWarnings(value="unused")
	private void solve() {
		KieServicesClient client = getClient();
		SolverServicesClient solverClient = client.getServicesClient(SolverServicesClient.class);

		try {
			solverClient.disposeSolver(CONTAINER_ID, SOLVER_ID);
		} catch (Exception e) {
		}

		EmployeeRoster unsolvedRoester = getSolution();

		SolverInstance solver = solverClient.createSolver(CONTAINER_ID, SOLVER_ID, CONFIG_FILE);
		solverClient.solvePlanningProblem(CONTAINER_ID, SOLVER_ID, unsolvedRoester);

	}

	@SuppressWarnings(value="unused")
	private void result() {
		KieServicesClient client = getClient();
		SolverServicesClient solverClient = client.getServicesClient(SolverServicesClient.class);
		SolverInstance solver = solverClient.getSolverWithBestSolution(CONTAINER_ID, SOLVER_ID);

		EmployeeRoster solution = (EmployeeRoster) solver.getBestSolution();
		System.out.println(solution);
		solverClient.disposeSolver(CONTAINER_ID, SOLVER_ID);

	}

	@SuppressWarnings(value="unused")
	private void getContainers() {
		KieServicesClient client = getClient();

		ServiceResponse<KieContainerResourceList> listContainers = client.listContainers();
		List<KieContainerResource> containers = listContainers.getResult().getContainers();
		containers.forEach(c -> {
			System.out.format("id: %s\n", c.getContainerId());
		});
	}

	private EmployeeRoster getSolution() {
		Skill skill = new Skill("chess");
		Employee employee = new Employee("Matej", Arrays.asList(skill));
		Timeslot timeslot = new Timeslot(LocalDateTime.of(2017, 1, 1, 0, 0), LocalDateTime.of(2017, 1, 1, 1, 0));
		Shift shift = new Shift(timeslot, skill);
		return new EmployeeRoster(	Arrays.asList(employee), Arrays.asList(shift), Arrays.asList(skill),
									Arrays.asList(timeslot), Collections.emptyList(), Collections.emptyList(), null);
	}

	private KieServicesClient getClient() {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(URL, user, password);
		//config.
		// Configuration for JMS
		// KieServicesConfiguration config =
		// KieServicesFactory.newJMSConfiguration(connectionFactory, requestQueue,
		// responseQueue, username, password)

		// Marshalling
		config.setMarshallingFormat(MarshallingFormat.JSON);
		Set<Class<?>> extraClasses = new HashSet<Class<?>>();
		extraClasses.add(HardSoftScore.class);
		extraClasses.add(EmployeeRoster.class);
		extraClasses.add(Employee.class);
		extraClasses.add(Shift.class);
		extraClasses.add(Skill.class);
		extraClasses.add(Timeslot.class);
		extraClasses.add(DayOffRequest.class);
		extraClasses.add(ShiftAssignment.class);

		config.addExtraClasses(extraClasses);
		Map<String, String> headers = null;
		config.setHeaders(headers);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

		return client;
	}

}
