Kie Client
==================================================

This project shows how to run a Business Optimizer solution (OptaPlanner) in two possible run-time configurations:

- **Embedded** the solver is executed in the same run-time of the calling API. See: `LocalKieContainerMain.java`.
- **Remote** the solver is executed in a *remote* run-time hosted by the kie-server. See `KieClientMain.java`.

Note that the Business Optimizer project does not require any change.

It's an employee rostering problem available in the project `employee-rostering` alongside this project.

The dependency is declared in the pom:

```xml
<dependency>
  <artifactId>employeerostering</artifactId>
  <groupId>employeerostering</groupId>
  <version>1.0.0-SNAPSHOT</version>
  <scope>compile</scope>
</dependency>
```

Remote Invocation
--------------------------------------------------

The project relies on the kie client APIs, that manage the protocol and marshalling details.

Note that by default it's selected the JSON serialization:

```java
config.setMarshallingFormat(MarshallingFormat.JSON);
```

The model classes need to be declared as *extra classes*, see:

```java
Set<Class<?>> extraClasses = new HashSet<Class<?>>();
extraClasses.add(HardSoftScore.class);
extraClasses.add(EmployeeRoster.class);
```

A special care requires the `LocalDateTime` used in the `employeerostering.employeerostering.Timeslot`:

```java
@JsonSerialize(using=LocalDateTimeSerializer.class)
@JsonDeserialize(using=LocalDateTimeDeserializer.class)
@JsonFormat(shape = Shape.STRING)
private java.time.LocalDateTime startTime;
```